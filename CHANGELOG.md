# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.2] - 2019-10-08
### Added
- Add command `:RSTTableCreateWithHeader` to create table with header row

### Changed
- Formatter maintains header row if already present

## [0.0.1] - 2019-10-08
### Changed
- Determining the starting and ending points of a block account for BOF and EOF.

## [0.0.0] - 2019-10-08
### Added
- This CHANGELOG file to hopefully serve as an evolving example of a
  standardized open source project CHANGELOG.
- Initial release.

[Unreleased]: https://gitlab.com/andrew.womeldorf/rst-table.vim/compare/0.0.2...master
[0.0.2]: https://gitlab.com/andrew.womeldorf/rst-table.vim/compare/0.0.1...0.0.2
[0.0.1]: https://gitlab.com/andrew.womeldorf/rst-table.vim/compare/0.0.0...0.0.1
[0.0.0]: https://gitlab.com/andrew.womeldorf/rst-table.vim/-/tags/0.0.0
