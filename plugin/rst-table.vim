" ============================================================================
" File:        rst-table.vim
" Description: Create an RST Table from a list of |-delimited strings
" Author:      Andrew Womeldorf <andrew DOT womeldorf AT gmail.com>
" Licence:     This file is placed in the public domain
" Website:     https://gitlab.com/andrew.womeldorf/rst-table.vim
" Version:     0.0.2
" ============================================================================

" avoid double loading
if exists('b:is_rst_table_loaded')
    finish
endif

" set special flag to prevent double loading on a buffer
let b:is_rst_table_loaded = 1
"
" Do not source this script when python is not compiled in.
if !has("python3")
    echomsg ":python3 is not available, rst-table will not be loaded."
    finish
endif

" Get the python script
execute 'py3file' fnamemodify(expand('<sfile>'), ':p:h:h') . '/python/table.py'

" Main worker
python3 rst_table = RST_Table()

" Commands
command! -range -nargs=? RSTTableCreate python3 rst_table.create()
command! -range -nargs=? RSTTableCreateWithHeader python3 rst_table.create(True)
command! -range -nargs=? RSTTableFormat python3 rst_table.format()
command! -range -nargs=? RSTTableRemove python3 rst_table.remove()
