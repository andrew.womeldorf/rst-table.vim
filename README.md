# RST Tables

## Introduction

Create and Format tables in ReStructured Text.

## Installation

**Requirements:**

- Vim compiled with Python 3 support

Installation can be done with [Vundle](https://github.com/VundleVim/Vundle.vim):

```
Plugin 'https://gitlab.com/andrew.womeldorf/rst-tables.vim'
```

Then, open Vim and run `:PluginInstall`.

## Usage

### Create a new table

Create a new rST Table from |-separated lines.  Assumes that the user has called
this function with a Visual Block selection.

Use `:RSTTableCreateWithHeader` to make the first line a header.

```
foo | bar | baz | banana
asdf | 9876543210 | !@#$%^ | x
aa | bbbbbbbbbbbbbbbbbbbbbbbbbbbbbb | c | xxxxxxxxxxxxxxx
```

Create a Visual Block over all of the rows with `V`, and call `:RSTTableCreate`.
Yields:

```
+------+--------------------------------+--------+-----------------+
| foo  | bar                            | baz    | banana          |
+------+--------------------------------+--------+-----------------+
| asdf | 9876543210                     | !@#$%^ | x               |
+------+--------------------------------+--------+-----------------+
| aa   | bbbbbbbbbbbbbbbbbbbbbbbbbbbbbb | c      | xxxxxxxxxxxxxxx |
+------+--------------------------------+--------+-----------------+
```

*Note: If the contents of your table are surrounded by empty lines,
`:RSTTableFormat` will create the table without the Visual Block.*

### Format an existing table

Reformat an existing rST Table.

Assumes that the user has called this function with their cursor placed
somewhere within the existing table.

```
+------+--------------------------------+--------+-----------------+
| foo  | bar                            | baz    | banana          |
+------+--------------------------------+--------+-----------------+
| asdf | 9876543210                     | !@#$%^&*() | x               |
+------+--------------------------------+--------+-----------------+
| aa   | bbbbbbbbbbbb | c      | xxxxxxxxxxxxxxx |
+------+--------------------------------+--------+-----------------+
```

Place your cursor anywhere in the table, and call `:RSTTableFormat`. Yields:

```
+------+--------------+------------+-----------------+
| foo  | bar          | baz        | banana          |
+------+--------------+------------+-----------------+
| asdf | 9876543210   | !@#$%^&*() | x               |
+------+--------------+------------+-----------------+
| aa   | bbbbbbbbbbbb | c          | xxxxxxxxxxxxxxx |
+------+--------------+------------+-----------------+
```

### Remove rST Table Formatting

Remove the rST Table formatting, and return a |-separated list.

The each element in the list is separated with as little whitespace
as possible, for cleanliness.

Assumes that the user has called this function with their cursor placed
somewhere within the existing table.

```
+------+--------------+------------+-----------------+
| foo  | bar          | baz        | banana          |
+------+--------------+------------+-----------------+
| asdf | 9876543210   | !@#$%^&*() | x               |
+------+--------------+------------+-----------------+
| aa   | bbbbbbbbbbbb | c          | xxxxxxxxxxxxxxx |
+------+--------------+------------+-----------------+
```

Place your cursor anywhere in the table, and call `:RSTTableRemove`. Yields:

```
foo | bar | baz | banana
asdf | 9876543210 | !@#$%^&*() | x
aa | bbbbbbbbbbbb | c | xxxxxxxxxxxxxxx
```

## Hotkey

For faster usage, I use these keymaps in my `vimrc`:

```
map <leader><leader>c :RSTTableCreate<CR>
map <leader><leader>h :RSTTableCreateWithHeader<CR>
map <leader><leader>f :RSTTableFormat<CR>
map <leader><leader>r :RSTTableRemove<CR>
```

## License

This plugin is released under the MIT License
