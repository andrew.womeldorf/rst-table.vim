import vim
import re

class RST_Table:
    """
    Easy table creating in ReStructured Text.
    """

    def create(self, withHeader = False):
        """
        Create a new rST Table from |-separated lines.

        Assumes that the user has called this function with a Visual Block
        selection.

        Example:
        foo | bar | baz | banana
        asdf | 9876543210 | !@#$%^ | x
        aa | bbbbbbbbbbbbbbbbbbbbbbbbbbbbbb | c | xxxxxxxxxxxxxxx

        yields:

        +------+--------------------------------+--------+-----------------+
        | foo  | bar                            | baz    | banana          |
        +------+--------------------------------+--------+-----------------+
        | asdf | 9876543210                     | !@#$%^ | x               |
        +------+--------------------------------+--------+-----------------+
        | aa   | bbbbbbbbbbbbbbbbbbbbbbbbbbbbbb | c      | xxxxxxxxxxxxxxx |
        +------+--------------------------------+--------+-----------------+
        """
        # block = (start, end)
        block = (
            vim.current.buffer.mark('<')[0]-1,
            vim.current.buffer.mark('>')[0]
        )

        # Extract the non-formatted lines from the buffer
        lines = self._getLinesFromBuffer(block[0], block[1])

        # Create the table
        newLines = self._writeTable(lines, withHeader)
            
        # Delete the current block
        del vim.current.buffer[block[0]:block[1]]

        # Write the new block
        vim.current.buffer.append(newLines, block[0])

    def format(self):
        """
        Reformat an existing rST Table.

        Assumes that the user has called this function with their cursor placed
        somewhere within the existing table.

        Pro-Tip: If the current block is not formatted as an rST table, and is
        surrounded by whitespace above and below, use this to create a table
        without visual block.

        Example:
        +------+--------------------------------+--------+-----------------+
        | foo  | bar                            | baz    | banana          |
        +------+--------------------------------+--------+-----------------+
        | asdf | 9876543210                     | !@#$%^&*() | x               |
        +------+--------------------------------+--------+-----------------+
        | aa   | bbbbbbbbbbbb | c      | xxxxxxxxxxxxxxx |
        +------+--------------------------------+--------+-----------------+

        yields:

        +------+--------------+------------+-----------------+
        | foo  | bar          | baz        | banana          |
        +------+--------------+------------+-----------------+
        | asdf | 9876543210   | !@#$%^&*() | x               |
        +------+--------------+------------+-----------------+
        | aa   | bbbbbbbbbbbb | c          | xxxxxxxxxxxxxxx |
        +------+--------------+------------+-----------------+
        """
        # block = (start, end)
        block = self._getBlockFromBuffer()

        # Determine if current table has a header
        withHeader = '+=' == vim.current.buffer[block[0]+2][0:2]

        # Remove the table formatting and get the raw lines again
        lines = self._removeTableFormatting(block[0], block[1])

        # Create the table
        newLines = self._writeTable(lines, withHeader)
            
        # Delete the current block
        del vim.current.buffer[block[0]:block[1]]

        # Write the new block
        vim.current.buffer.append(newLines, block[0])

    def remove(self):
        """
        Remove the rST Table formatting, and return a |-separated list.

        The each element in the list is separated with as little whitespace
        as possible, for cleanliness.

        Assumes that the user has called this function with their cursor placed
        somewhere within the existing table.

        Example:
        +------+--------------+------------+-----------------+
        | foo  | bar          | baz        | banana          |
        +------+--------------+------------+-----------------+
        | asdf | 9876543210   | !@#$%^&*() | x               |
        +------+--------------+------------+-----------------+
        | aa   | bbbbbbbbbbbb | c          | xxxxxxxxxxxxxxx |
        +------+--------------+------------+-----------------+

        yields:

        foo | bar | baz | banana
        asdf | 9876543210 | !@#$%^&*() | x
        aa | bbbbbbbbbbbb | c | xxxxxxxxxxxxxxx
        """
        # block = (start, end)
        block = self._getBlockFromBuffer()

        # Remove the table formatting and get the raw lines again
        lines = self._removeTableFormatting(block[0], block[1], True)
            
        # Delete the current block
        del vim.current.buffer[block[0]:block[1]]

        # Write the new block
        vim.current.buffer.append(lines, block[0])

    def _writeTable(self, lines, withHeader = False):
        """
        Given a list of strings, write out the rST table, properly formatted.
        """
        widths = self._getColumnWidths(lines)
        rowSeparator = self._createRowSeparator(widths)

        # This is what will replace the range
        newLines = []

        # Create the first line of the table
        newLines.append(rowSeparator)

        # Format each line to have column endings at same place
        for l, line in enumerate(lines):
            columns = line.split('|')

            for i, col in enumerate(columns):
                columns[i] = f'| {col.strip():{widths[i]}} '

            columns.append('|')

            newLines.append(''.join(columns))

            if 0 == l:
                newLines.append(self._createRowSeparator(widths, withHeader))
            else:
                newLines.append(rowSeparator)

        return newLines

    def _removeTableFormatting(self, start, end, clean = False):
        """
        Remove the rST Table formatting, to go back to | separated lines
        """
        lines = vim.current.buffer[start:end]

        i = len(lines)
        while True:
            i -= 1
            if 0 > i:
                break

            if '+' == lines[i][0]:
                del lines[i]
            elif clean:
                columns = lines[i].strip('|').split('|')
                columns = [col.strip() for col in columns]
                lines[i] = ' | '.join(columns)
            else:
                lines[i] = lines[i].strip('|')

        return lines

    def _getBlockFromBuffer(self):
        """
        Determine the starting and ending point of the table.

        Assumes vim cursor is on a row inside an existing rST Table, which is
        surrounded by empty lines.

        Since 0.0.1, accounts for BOF and EOF.
        """
        # [start, end]
        tableRange = [0, 0]

        # set starting position
        current = vim.current.window.cursor[0]
        while True:
            if 0 > current:
                tableRange[0] = current + 1
                break
            elif '' == vim.current.buffer[current]:
                tableRange[0] = current + 1
                break
            current -= 1

        # set ending position
        current = vim.current.window.cursor[0]
        while True:
            if len(vim.current.buffer) == current:
                tableRange[1] = current
                break
            elif '' == vim.current.buffer[current]:
                tableRange[1] = current
                break
            current += 1

        return tuple(tableRange)

    def _getLinesFromBuffer(self, start, end):
        """
        Extract the content from the visual block in the current Vim buffer.
        """
        lines = []

        for i in range(start, end):
            lines.append(vim.current.buffer[i])

        return lines

    def _getColumnWidths(self, lines):
        """
        Loop over each line, split the line by `|`, and determine the max length
        of each column.
        """
        widths = []

        for line in lines:
            columns = line.split('|')
            
            for i, col in enumerate(columns):
                # Strip whitespace from beginning/end
                col = col.strip()

                # Have no length for this column
                if i >= len(widths):
                    widths.append(len(col))

                # This is longer than current max
                elif len(col) > widths[i]:
                    widths[i] = len(col)

        return widths # int[]

    def _createRowSeparator(self, widths, header = False):
        """
        Create the row separator string.
        """
        ret = ''
        char = '=' if header else '-'

        for col in widths:
            ret += f'+{char:{char}<{col+1}}{char}'
        ret += '+'

        return ret # +-----------+------+
